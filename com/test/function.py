'''
Created on Aug 25, 2017

@author: suresh
'''
#this file contain function

# my function
def  printnumbers(number =225 , name='suresh' ,age=26):    #default argument number=20
     print (number,name,age)
#     num=number*10
#     return num
# print ("detail0 is" , printnumbers()) 
# print ("detail1 is" , printnumbers(number=226,name='yasar')) 
# print ("detail is " , printnumbers(226,'yasar',26)) 

printnumbers() 
printnumbers(number=226,name='yasar') 
printnumbers(226,'yasar',27)



#multiple arguments
def calculatnumbers(*args):
    total =0;
    for  i in args:
        total +=i
    print("the total numer is",total)
        
        
calculatnumbers(10,56)       


#unpacked arguments

def lifetime(age,drink,smoke):
    age=(100-age)-(drink*2)-(smoke*2)
    print("total age u are living is", age)

    
lifetime(26, 1, 10) 

suresh = [26,1,10]
kumar = [26,0,0]

lifetime(suresh[0], suresh[1], suresh[2])
lifetime(*kumar)  #unpacking arguments (*kumar)



groovy ={'suresh','kumar','suresh','kumar','good'} # sets dont have any duplicate 

print("Sets dont have duplicate",groovy)
     
