
# class Robot:
# 	pass

# x=Robot()
# y=Robot()

# x.name="name52"

# y.name="namey"

# print(x.__dict__)


class Robot:
 
    def __init__(self, name=None):
        self.name = name   
        
    def say_hi(self):
        if self.name:
            print("Hi, I am " + self.name)
        else:
            print("Hi, I am a robot without a name")
    

x = Robot()
x.say_hi()
y = Robot("Marvin")
y.say_hi()