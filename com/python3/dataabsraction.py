class suresh:

	def __init__(self,name="kumar",year="1992"):
		self.name=name
		self.year=year

	def sayHi(self):
		print(self.name)
		print(self.year)

	def get_name(self):
		return self.name

	def get_year(self):
		return self.year

	def set_name(self,na):
		self.name=na

	def set_year(self,ye):
		self.year=ye


s=suresh()
s.set_name("suresh")
s.set_year('1992')
s.sayHi()

