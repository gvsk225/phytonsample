class suresh:

     def __init__(self,name=None,name1=None,name2=None):
         self.name=name
         self._name1=name1
         self.__name2=name2

     def print(self):
        print (self.name,self._name1,self.__name2)

     def get_name(self):
         return self.name
 
     def get_name1(self):
         return self._name1
 
     def get_name2(self):
         return self.__name2 
 
     def set_name(self,name):
         self.name=name

     def set_name1(self,name1):
         self._name1=name1

     def set_name2(self,name2):
         self.__name2=name2


if __name__ == '__main__':
    x=suresh()
    x.set_name("suresh public")
    x.set_name1("suresh Proctected")
    x.set_name2("suresh private")
    x.print() 



